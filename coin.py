#!/bin/env python3
import random, sys

def coinFlip():
    return random.choice(["heads", "tails"])

if __name__ == "__main__":
    print(coinFlip())
